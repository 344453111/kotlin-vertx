package common;

import kotlin.Unit;
import kotlin.coroutines.experimental.Continuation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by JinMiao
 * 2017/11/7.
 */
public abstract class IJavaHander extends ExceptionHandler {


    @Nullable
    @Override
    public Object execute(@NotNull Message msg, @NotNull Continuation<? super Unit> continuation) throws Throwable {
        handler(msg);
        return null;
    }

    public abstract void handler(Message msg) throws Throwable;
}
