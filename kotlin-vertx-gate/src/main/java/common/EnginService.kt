package common

import code.HandlerManager
import code.IHandlerManager
import code.PBMessageManager
import handler.ClientHeartBeatHandler
import handler.ServerHeartBeatHandler
import io.vertx.core.Vertx
import threadPool.MessageDispatch

open class EnginService {
    /**消息体管理类 获得消息体用 */
    var messageManager= PBMessageManager()
    /**业务处理类管理类 */
    val handlerManager: IHandlerManager = HandlerManager()
    /**channel管理类 */
    val channelManager: ChannelManager = ChannelManager()

    var vertx: Vertx

    lateinit var messageDispatch: MessageDispatch

    constructor(vertx: Vertx) {
        this.vertx = vertx
        init()
    }

    private fun init() {
        handlerManager.addHandler(Message.M_ID, Message.H_SEND_HEART_BEAT, ServerHeartBeatHandler())
        handlerManager.addHandler(Message.M_ID, Message.H_RECIEVE_HEART_BEAT, ClientHeartBeatHandler())
    }









}
