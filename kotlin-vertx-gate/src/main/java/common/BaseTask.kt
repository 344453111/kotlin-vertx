package common

import org.slf4j.LoggerFactory

/**
 * Created by JinMiao
 * 2017/10/20.
 */
class BaseTask: ITask {

    private val msg: Message
    private val handler: IKtHandler?

    constructor(msg: Message, handler: IKtHandler?){
        this.msg = msg
        this.handler = handler
    }

    suspend override fun run() {
        if (handler == null)
        {
            log.error("no exist handler，mid:" + msg.getmId() + "  " + msg.gethId())
            return
        }
        handler.handler(msg)
    }

    companion object {
        private val log = LoggerFactory.getLogger(BaseTask::class.java)
    }
}