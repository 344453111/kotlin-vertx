package code

import common.IKtHandler

/**
 * 业务处理类管理类
 * @author King
 */
interface IHandlerManager {

    /**
     * 增加一个业务处理类
     * @param modelId
     * @param actionId
     * @param handler
     */
    fun addHandler(modelId: Int, actionId: Int, handler: IKtHandler)

    /**
     * 获取一个业务处理类
     * @param modelId
     * @param actionId
     * @return
     */
    fun getHandler(modelId: Int, actionId: Int): IKtHandler?

}