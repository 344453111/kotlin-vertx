package code

import io.netty.channel.socket.SocketChannel
import io.netty.handler.timeout.IdleStateHandler

/**
 * 服务器用的
 * @author King
 */
class ServerChannelInitializer : AbsChannelInitializer() {
    @Throws(Exception::class)
    override fun initChannel(ch: SocketChannel) {
        val pipeline = ch.pipeline()
        //tcp丢包断包处理  消息头增加4字节标记消息长度
        pipeline.addLast("frameDecoder", MyLengthFieldBasedFrameDecoder(Integer.MAX_VALUE, 0, 0))
        pipeline.addLast("frameEncoder", MyLengthFieldPrepender())
        val decode = PBBytesToMessageDecode()
        decode.setMessageManager(this.service.messageManager)
        pipeline.addLast("byteToMyMessageDecoder", decode)
        //encode
        pipeline.addLast("myMessageToByteEncoder", PBMssageToBytesEncode())
        //心跳
        pipeline.addLast(IdleStateHandler(30, 0, 0))
        val handler = ServerChannelHandler()
        handler.service=this.service
        pipeline.addLast("liangShanHandler", handler)
    }
}
