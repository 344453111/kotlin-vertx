package code

import common.EnginService
import io.netty.channel.ChannelInitializer
import io.netty.channel.socket.SocketChannel

/**
 * channel filter抽象类
 * @author King
 */
abstract class AbsChannelInitializer : ChannelInitializer<SocketChannel>() {

    protected lateinit var service: EnginService

    fun setEnginService(service: EnginService) {
        this.service = service
    }
}
