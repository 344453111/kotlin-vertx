package utils

import common.ITask
import kotlinx.coroutines.experimental.channels.ActorJob
import kotlinx.coroutines.experimental.runBlocking

/**
 * Created by JinMiao
 * 2017/11/13.
 */

fun ActorJob<ITask>.sendTask(handler: ITask){
    if (offer(handler))
        return
    runBlocking {
        send(handler)
    }
}