package serialization;

import java.io.IOException;

import org.msgpack.MessagePack;
/**
 * msgpack序列化
 * @author jinmiao
 *
 */
public class MsgPackSerialize implements ISerialize{
	
    private static final MessagePack msgpack = new MessagePack();
    
    @Override
    public byte[] serialize(Object obj) {
	try {
		return msgpack.write(obj);
	} catch (IOException e) {
	    throw new RuntimeException(e);
	}
    }

	@Override
    public <T> T unSerialize(byte[] byts, Class<T> t) {
	if(byts==null)
	    return null;
	try {
	    return msgpack.read(byts, t);
	} catch (IOException e) {
	    throw new RuntimeException(e);
	}
    }
}
