package serialization;

import java.nio.charset.Charset;

import com.alibaba.fastjson.JSON;

public class JsonSerialize implements ISerialize
{
	public static final Charset charset_utf8 = Charset.forName("UTF-8");
	@Override
	public byte[] serialize(Object obj) {
		return JSON.toJSONString(obj).getBytes(charset_utf8);
	}

	@Override
	public <T> T unSerialize(byte[] byts, Class<T> t) {
		if(byts==null)
			return null;
		String str = new String(byts, charset_utf8);
		return JSON.parseObject(str, t);
	}

}
