package serialization;
/**
 * 序列化与反序列化接口
 * @author Administrator
 *
 */
public interface ISerialize {
	
	public byte[] serialize(Object obj);
	
	public <T> T unSerialize(byte[] byts,Class<T> t);
}
