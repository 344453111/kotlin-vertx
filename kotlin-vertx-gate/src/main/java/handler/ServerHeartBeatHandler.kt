package handler

import common.ExceptionHandler
import common.Message

/**
 * 服务器收到心跳
 * @author pc
 */
class ServerHeartBeatHandler : ExceptionHandler() {
    suspend override fun execute(msg: Message) {
        val channel = msg.channel
        channel!!.isHeartBeat = false
        //发送心跳返回包
        val nMsg = Message.newMessage()
        nMsg.setmId(Message.M_ID)
        nMsg.sethId(Message.H_RECIEVE_HEART_BEAT)
        channel.write(nMsg)
    }


    override fun initBodyClass(): Class<Any>? {
        return null
    }
}
