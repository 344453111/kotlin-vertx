package handler

import common.ExceptionHandler
import common.Message

/**
 * 客户端心跳包
 * @author pc
 */
class ClientHeartBeatHandler : ExceptionHandler() {
    suspend override fun execute(msg: Message) {
        val channel = msg.channel
        channel!!.isHeartBeat = false
    }

    override fun initBodyClass(): Class<Any>? {
        return null
    }

}
